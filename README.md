# Assignment 1 RPG Character

A small java console app for RPG characters where you:
 - Create your character
 - Level up your character
 - Equip your character with weapons
 - Equip your character with armor
# Installation
To run this program you need to have a Java Development Kit and IDE to your preference installed.

# How to use
Right click Run on the Main.class
![image.png](./image.png)

## Contributions

Thank you Savanna for helping me out with fine tuning.
