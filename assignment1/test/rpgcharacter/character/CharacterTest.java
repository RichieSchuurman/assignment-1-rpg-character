package rpgcharacter.character;

import rpgcharacter.character.Mage;
import rpgcharacter.character.Ranger;
import rpgcharacter.character.Rogue;
import rpgcharacter.character.Warrior;
import rpgcharacter.exceptions.InvalidArmorException;
import rpgcharacter.exceptions.InvalidWeaponException;
import rpgcharacter.items.Armor;
import rpgcharacter.items.Item;
import rpgcharacter.items.Slot;
import rpgcharacter.items.Weapon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {
    Ranger rangerTest;
    Mage mageTest;
    Rogue rogueTest;
    Warrior warriorTest;

    @BeforeEach
    void setUp() {
        rangerTest = new Ranger("rangerTest");
        mageTest = new Mage("mageTest");
        rogueTest = new Rogue("rogueTest");
        warriorTest = new Warrior("warriorTest");
    }

    @Test
    void getStartLevel_ValidInputs_shouldReturnOne() {
        int expected = 1;
        int actual = rangerTest.getLevel();
        assertEquals(expected, actual);
    }

    @Test
    void levelUp_validInputs_shouldReturnTwo() {
        int expected = 2;
        rangerTest.levelUp();
        int actual = rangerTest.getLevel();
        assertEquals(expected, actual);
    }

    @Test
    void rangerDefaultAttributes_validInputs_ShouldReturnOneSevenOne() {
        String expected = "1,7,1";
        String actual = rangerTest.getPrimaryAttributes();
        assertEquals(expected, actual);
    }



    @Test
    void rangerAttributeIncrease_validInputs_shouldReturnTwoTwelveTwo(){
        rangerTest.levelUp();
        String expected = "2,12,2";
        String actual = rangerTest.getPrimaryAttributes();
        assertEquals(expected, actual);
    }

    @Test
    void highLevelWeapon_invalidInputs_shouldThrowInvalidWeaponException() {
        Weapon testWeapon = new Weapon("Common Bow", 2, Slot.WEAPON, Weapon.Type.BOW, 5, 3.1);
        String expected = "You're not able to wield this weapon.";

        Exception exception = assertThrows(InvalidWeaponException.class, () -> rangerTest.equipWeapon(testWeapon));
        String actual = exception.getMessage();

        assertEquals(expected, actual);
    }

    @Test
    void calculateDPSNoWeapon_validInputs_shouldReturnDPS() {
        double expected = 1 * (1 + (5 / 100));
        double actual = rangerTest.getCharacterDPS();
        assertEquals(expected, actual);
    }

    @Test
    void calculateDPSWithWeapon_validInputs_shouldReturnDPS() throws InvalidWeaponException {
        Weapon testWeapon = new Weapon("Common Bow", 1, Slot.WEAPON, Weapon.Type.BOW, 9, 4.1);
        rangerTest.equipWeapon(testWeapon);

        double expected = (7 * 1.1)*(1 + (5 / 100));
        double actual = rangerTest.getCharacterDPS();
        assertEquals(expected, actual);
    }
}