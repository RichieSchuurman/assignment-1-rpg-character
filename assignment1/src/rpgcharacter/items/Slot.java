package rpgcharacter.items;

public enum Slot {
    HEAD,
    BODY,
    ARMS,
    LEGS,
    WEAPON
}
