package rpgcharacter.items;

import rpgcharacter.attributes.PrimaryAttributes;

public class Armor extends Item {
    private PrimaryAttributes armorAttributes;

    //Credits to: https://stackoverflow.com/questions/48627744/how-do-you-implement-an-enum-in-an-object-class-java/48627811
    public enum Type {
        CLOTH,
        LEATHER,
        MAIL,
        PLATE
    }

    public Armor(String name, int levelNeeded, Slot itemSlot, Armor.Type type, PrimaryAttributes armorAttributes) {
        super(name, levelNeeded, itemSlot, type);
        this.armorAttributes = new PrimaryAttributes(armorAttributes.getStrength(), armorAttributes.getDexterity(), armorAttributes.getIntelligence());
    }
}
