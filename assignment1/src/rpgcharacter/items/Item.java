package rpgcharacter.items;

public abstract class Item {

    //Class fields
    private String name;
    private int levelNeeded;
    private Slot itemSlot;
    private Weapon.Type weaponType;
    private Armor.Type armorType;

    //Weapon constructor
    public Item(String name, int levelNeeded, Slot itemSlot, Weapon.Type type) {
        this.name = name;
        this.levelNeeded = levelNeeded;
        this.itemSlot = itemSlot;
        this.weaponType = type;
    }

    //Armor constructor
    public Item(String name, int levelNeeded, Slot itemSlot, Armor.Type type) {
        this.name = name;
        this.levelNeeded = levelNeeded;
        this.itemSlot = itemSlot;
        this.armorType = type;
    }

    //Getters
    public String getName() {
        return name;
    }

    public int getLevelNeeded() {
        return levelNeeded;
    }

    public Slot getSlot() {
        return itemSlot;
    }

    public Weapon.Type getWeaponType() {
        return weaponType;
    }

    public Armor.Type getArmorType() {
        return armorType;
    }




}
