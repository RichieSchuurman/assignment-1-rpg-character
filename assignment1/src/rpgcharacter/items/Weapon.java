package rpgcharacter.items;

public class Weapon extends Item {
    private double weaponDPS;

    //Credits to: https://stackoverflow.com/questions/48627744/how-do-you-implement-an-enum-in-an-object-class-java/48627811
    public enum Type {
        AXE,
        BOW,
        DAGGER,
        HAMMER,
        STAFF,
        SWORD,
        WAND
    }

    //Constructor
    public Weapon(String name, int levelNeeded, Slot itemSlot, Type type, int damage, double attackSpeed) {
        super(name, levelNeeded, itemSlot, type);
        this.weaponDPS = damage * attackSpeed;
    }

    public double getWeaponDPS() {
        return weaponDPS;
    }
}
