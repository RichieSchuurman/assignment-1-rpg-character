package rpgcharacter.attributes;

public class PrimaryAttributes {

    //Class fields
    private int strength;
    private int dexterity;
    private int intelligence;

    //Constructor
    public PrimaryAttributes(int strength, int dexterity, int intelligence) {
        setStrength(strength);
        setDexterity(dexterity);
        setIntelligence(intelligence);
    }

    //Getters
    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    //Setters
    public void setStrength(int strength) {
        this.strength = this.strength;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }
}
