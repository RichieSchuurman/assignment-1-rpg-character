package rpgcharacter;

import rpgcharacter.character.Ranger;
import rpgcharacter.items.Armor;
import rpgcharacter.items.Slot;
import rpgcharacter.items.Weapon;

public class Main {

    public static void main(String[] args) throws Exception {
        Ranger atalanta = new Ranger("Atalanta");

        Weapon howlingBow = new Weapon("Howling Bow", 1, Slot.WEAPON, Weapon.Type.BOW, 6, 10.0);
        atalanta.equipWeapon(howlingBow);

        //Armor leatherLeggings = new Armor("Leather leggings", 1, Slot.BODY, Armor.Type.LEATHER, "Leather Leggings", 6, 3, 5);
        //atalanta.equipArmor(leatherLeggings);

        atalanta.characterStats();
    }
}
