package rpgcharacter.character;

import rpgcharacter.attributes.PrimaryAttributes;
import rpgcharacter.exceptions.InvalidArmorException;
import rpgcharacter.exceptions.InvalidWeaponException;
import rpgcharacter.items.Armor;
import rpgcharacter.items.Weapon;

public class Ranger extends Character {

    //Constructor
    public Ranger(String name) {
        super(name, new PrimaryAttributes(1, 7, 1));
    }

    public void levelUp() {
        levelUp(1, 5, 1);
    }

    public double calculateCharacterDPS(int dexterity) {
        return calculateCharacterDPS(getTotalPrimaryAttributes().getDexterity());
    }

    public void equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if (weapon.getWeaponType() == Weapon.Type.BOW && weapon.getLevelNeeded() <= this.getLevel()) {
          setEquipment(weapon);
          setCharacterDPS("dexerity");
        } else {
            throw new InvalidWeaponException("You're not able to wield this weapon.");
        }
    }

    public void equipArmor(Armor armor) throws InvalidArmorException {
        if (armor.getArmorType() == Armor.Type.LEATHER || armor.getArmorType() == Armor.Type.MAIL && armor.getLevelNeeded() <= this.getLevel()) {
            setEquipment(armor);
            setCharacterDPS("dexterity");
        } else {
            throw new InvalidArmorException("You're not able to equip this armor.");
        }
    }

}
