package rpgcharacter.character;

import rpgcharacter.attributes.PrimaryAttributes;
import rpgcharacter.exceptions.InvalidArmorException;
import rpgcharacter.exceptions.InvalidWeaponException;
import rpgcharacter.items.Armor;
import rpgcharacter.items.Item;
import rpgcharacter.items.Slot;
import rpgcharacter.items.Weapon;

import java.util.HashMap;

public abstract class Character {

    //Class fields
    private String name;
    private int level;
    private PrimaryAttributes primaryAttributes;
    private PrimaryAttributes totalPrimaryAttributes;
    private double characterDPS;
    private HashMap<Slot, Item> equipment;

    //Constructor
    public Character(String name, PrimaryAttributes primaryAttributes) {
        this.name = name;
        this.level = 1;
        this.primaryAttributes = primaryAttributes;
        this.equipment = new HashMap<Slot, Item>();
    }

    //Getters
    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public PrimaryAttributes getPrimaryAttributes() {
        return primaryAttributes;
    }

    public PrimaryAttributes getTotalPrimaryAttributes() {
        return totalPrimaryAttributes;
    }

    public HashMap<Slot, Item> getEquipment() {
        return equipment;
    }

    public double getCharacterDPS() {
        return characterDPS;
    }

    //Setters
    public void setName(String name) {
        this.name = name;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void setPrimaryAttributes(PrimaryAttributes primaryAttributes) {
        this.primaryAttributes = primaryAttributes;
    }

    public void setTotalPrimaryAttributes(PrimaryAttributes totalPrimaryAttributes) {
        this.primaryAttributes = totalPrimaryAttributes;
    }

    public void setEquipment(HashMap<Slot, Item> equipment) {
        this.equipment = equipment;
    }

    //Credits to Savannah Borst for helping me out
    public void setCharacterDPS(String main) {
        getTotalPrimaryAttributes();
        //  initialize mainAttr
        int mainAttribute = 0;
        // check which is main attribute + get.
        if  (main.equals("strength")){
            mainAttribute = totalPrimaryAttributes.getStrength();
        }  else if (main.equals("dexterity")) {
            mainAttribute = totalPrimaryAttributes.getDexterity();
        } else if (main.equals("intelligence")) {
            mainAttribute = totalPrimaryAttributes.getIntelligence();
        }
        // if weapon slot is not empty
        if (this.equipment.get(Slot.WEAPON) != null) {
            double weaponDPS = ((Weapon) this.equipment.get(Slot.WEAPON)).getWeaponDPS();
            this.characterDPS = weaponDPS * (1 + (mainAttribute / 100));
        } else {
            this.characterDPS = (1 + (mainAttribute / 100));
        }
    }

    //Methods
    public void levelUp(int strength, int dexterity, int intelligence) {
        setLevel(this.level += 1);
        updatePrimaryAttributes(strength, dexterity, intelligence);
    }

    public void updatePrimaryAttributes(int strength, int dexterity, int intelligence) {
        setPrimaryAttributes(new PrimaryAttributes(
                this.primaryAttributes.getStrength() + strength,
                this.primaryAttributes.getDexterity() + dexterity,
                this.primaryAttributes.getIntelligence() + intelligence
        ));
    }

    public void updateTotalPrimaryAttributes(PrimaryAttributes armorPrimaryAttribute) {
        setTotalPrimaryAttributes(new PrimaryAttributes(
                this.primaryAttributes.getStrength() + armorPrimaryAttribute.getStrength(),
                this.primaryAttributes.getDexterity() + armorPrimaryAttribute.getDexterity(),
                this.primaryAttributes.getIntelligence() + armorPrimaryAttribute.getIntelligence()
        ));
    }

    public void setEquipment(Weapon weapon) throws InvalidWeaponException {
        if(weapon.getLevelNeeded() <= this.level) {
            equipment.put(weapon.getSlot(), weapon);
        } else {
            throw new InvalidWeaponException("Sorry, the level of this weapon is currently too high to wield.");
        }
    }

    public void setEquipment(Armor armor) throws InvalidArmorException {
        if(armor.getLevelNeeded() <= this.level) {
            equipment.put(armor.getSlot(), armor);
        } else {
            throw new InvalidArmorException("Sorry, the level of this armor is currently too high to equip");
        }
    }

    public String characterStats() {
        StringBuilder characterStats = new StringBuilder();

        return characterStats.append(
                "Character: " + this.getName() + "\n" +
                        "Level: " + this.getLevel() + "\n" +
                        "Strength: " + this.getTotalPrimaryAttributes().getStrength() + "\n" +
                        "Dexterity: " + this.getTotalPrimaryAttributes().getDexterity() + "\n" +
                        "Intelligence: " + this.getTotalPrimaryAttributes().getIntelligence() + "\n" +
                        "CharacterDPS: " + this.getCharacterDPS()).toString();
    }
}
