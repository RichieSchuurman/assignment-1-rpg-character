package rpgcharacter.character;

import rpgcharacter.attributes.PrimaryAttributes;
import rpgcharacter.exceptions.InvalidArmorException;
import rpgcharacter.exceptions.InvalidWeaponException;
import rpgcharacter.items.Armor;
import rpgcharacter.items.Weapon;

public class Warrior extends Character {
    public Warrior(String name) {
        super(name, new PrimaryAttributes(5, 2, 1));
    }

    public void levelUp() {
        levelUp(3, 2, 1);
    }

    public double calculateCharacterDPS(int strength) {
        return calculateCharacterDPS(getTotalPrimaryAttributes().getStrength());
    }

    public void equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if (weapon.getWeaponType() == Weapon.Type.AXE ||weapon.getWeaponType() == Weapon.Type.HAMMER || weapon.getWeaponType() == Weapon.Type.SWORD && weapon.getLevelNeeded() <= this.getLevel()) {
            setEquipment(weapon);
            setCharacterDPS("strength");
        } else {
            throw new InvalidWeaponException("You're not able to wield this weapon.");
        }
    }

    public void equipArmor(Armor armor) throws InvalidArmorException {
        if (armor.getArmorType() == Armor.Type.MAIL || armor.getArmorType() == Armor.Type.PLATE && armor.getLevelNeeded() <= this.getLevel()) {
            setEquipment(armor);
            setCharacterDPS("dexterity");
        } else {
            throw new InvalidArmorException("You're not able to equip this armor.");
        }
    }
}
